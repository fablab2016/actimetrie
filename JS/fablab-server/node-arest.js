/**
 * Created by marcelo on 24/03/16.
 */
var express = require('express');
var ip = require('ip');
var exec = require('exec');
var app = express();
// Rest
var rest = require("arest")(app);

// Define port
var port = 3001;



app.listen(port);
console.log("Listening on port " + port);

module.exports = function(db, session){
    var intervalID = null;

    var runSetup = function(){

        console.log("Running Command: nmap -n -sn "+ ip.address() + "/24 -oG - | awk '/Up$/{print $2}'");
        child = exec("nmap -n -sn "+ ip.address() + "/24 -oG - | awk '/Up$/{print $2}'",
            function(error, stdout, stderr) {
                var ipList = stdout.split("\n");
                var myAddrSplit = ip.address().split('.');
                var routerAddr = myAddrSplit[0] + '.' + myAddrSplit[1] + '.' + myAddrSplit[2] + '.1';
                for(var i = 0; i < ipList.length - 1; i++){ // Last one is empty
                    if(ipList[i] !== ip.address() && ipList[i] !== routerAddr){ //Ignore server and router
                        addBoard(ipList[i]);
                    }
                }
            });
    };

    var addBoard = function(boardIp){
        var collection = db.get('boardcollection');

        console.log(session + " " + boardIp);
        collection.insert({
            'session': session,
            'ip' : boardIp,
            'name': "" //Empty name to be set by user
        }, function(err, doc){
            if(err){
                //console.log("There was a problem adding info to database"); Already in DB
            } else {
                console.log("Board with ip: " + boardIp + " was added");
            }
        })
    };

    var getDataProc;

    return {
        startSetup: function(){
            runSetup();
            intervalID = setInterval(runSetup, 10000);
        },

        finishSetup: function(){
            if(intervalID != null){
                clearInterval(intervalID);
            }
        },

        runDataGet: function(){
            console.log("Running Command: python get-data.py "+session);
            getDataProc = exec("python get-data.py "+session,
                function(error, stdout, stderr) {
                    console.log(stdout);
                    if(error){
                        console.log(stderr);
                    }
                });
        },

        finishDataGet: function(){
            getDataProc.kill();
        },

        insertBoard: function(boardIp){ //Manual insertion
            //TODO
            console.log("TODO validateIp and add Board");
        }
    }
};


