/**
 * Created by marcelo on 19/03/16.
 */

var express = require('express');
var router = express.Router();

router.get('/setup', function(req, res, next) {
        res.render('debugSetup', { title: 'Setup Debug' });
});

router.get('/sessions', function(req, res, next){
    var db = req.db;
    var collection = db.get('sessioncollection');
    collection.find({}, {},function(e, docs) {
        var sessionlist = [];
        for(var i = 0; i < docs.length; i++){
            var date = new Date(docs[i].session);
            console.log(date);
            sessionlist[sessionlist.length] = {'session':docs[i].session,'date': date.toLocaleDateString(), 'time': date.toLocaleTimeString()}
        }
        res.render('debugSession', {title: 'Session Debug', sessionlist: sessionlist});
    });
});

router.post('/addBoard', function(req, res){
    var db = req.db;
    var ip = req.body.boardIp;
    var name = req.body.boardName;
    var collection = db.get('boardcollection');

    collection.insert({
        'session': req.session,
        'ip' : ip,
        'name': name
    }, function(err, doc){
        if(err){
            res.send("There was a problem adding info to database");
        } else {
            res.render('debugSetup', { title: 'Setup Debug' });
        }
    })
});



module.exports = router;