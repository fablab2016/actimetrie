/**
 * Created by marcelo on 21/03/16.
 */

angular.module('app',['notifications','ngResource', 'ngRoute', 'btford.socket-io', 'ui.bootstrap', 'angular-flot'])
    .constant('States', {
        'IDLE':     {   'name': "IDLE",
                        'path': '/' },
        'SETUP':    {   'name': "SETUP",
                        'path': '/setup' },
        'RUNNING':  {   'name': "RUNNING",
                        'path': '/run' }
    })
    .factory('currentState', function($location, socket, States){
        var state = States.IDLE.name;
        var sessionId;
        var boards = [];

        socket.on('startSetup', function(id){
            console.log("setup");
            sessionId = id;
            state = States.SETUP.name;
            updatePath();
        });
        socket.on('finishSetup', function(){
            console.log("running");
            state = States.RUNNING.name;
            updatePath();
        });

        var updatePath = function(){
            console.log(state);
            console.log(States[state].path);
            $location.path(States[state].path);
        };

        return {
            get: function(){
                return state;
            },
            startSetup: function(){
                state = States.SETUP.name;
                updatePath();
                socket.emit('startSetup');
            },
            finishSetup: function(){
                state = States.RUNNING.name;
                updatePath();
                socket.emit('finishSetup');
            },
            setSetup: function(s){
                state = s;
                updatePath();
            },
            getSessionId: function(){
                return sessionId;
            },
            setBoards: function(b){
                boards = b;
            },
            getBoards: function(){
                return boards;
            }
        }
    })
    .config(function($routeProvider, $locationProvider, States){
        $routeProvider
            .when('/', {
                templateUrl: 'pages/idleIndex',
                controller: 'beginCtrl',
                resolve:{
                    'check': function($location, currentState, States){
                        var curPage = currentState.get();
                        if(curPage !== States.IDLE.name){
                            $location.path(States[curPage].path);
                        }
                    }
                }
            })
            .when('/setup', {
                templateUrl: 'pages/setup',
                controller: 'setupCtrl',
                resolve:{
                    'check': function($location, currentState, States){
                        var curPage = currentState.get();
                        if(curPage !== States.SETUP.name){
                            $location.path(States[curPage].path);
                        }
                    }
                }
            })
            .when('/run', {
                templateUrl: 'pages/run',
                controller: 'runCtrl',
                resolve: {
                    'check': function($location, currentState, States) {
                        var curPage = currentState.get();
                        if (curPage !== States.RUNNING.name) {
                            $location.path(States[curPage].path);
                        }
                    }
                }
            })
            .when('/recorded', {
                templateUrl: 'pages/recorded',
                controller: 'recordedCtrl'
            })
            .when('/sessions', {
                templateUrl: 'pages/sessions',
                controller: 'sessionsCtrl'
            })
            .otherwise({
                redirectTo: States.IDLE.path
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })
    .factory('socket', function(socketFactory){
        return socketFactory();
    })
    .controller('menuCtrl', ['$scope', 'socket', 'currentState', function($scope, socket, currentState){
        $scope.connection = function(){
            socket.emit('init');
        };
        socket.on('init',function(initData){
            $scope.serverSession = initData.session;
            currentState.setSetup(initData.setupState);
        });
    }])
    .controller('beginCtrl', ['$scope', 'socket', 'currentState', 'States', function($scope, socket, currentState, States){
        $scope.startSetup = function(){
            currentState.startSetup();
            console.log('setup starting...');
        };
    }])
    .controller('setupCtrl', ['$scope', '$notification', '$http', 'socket', 'currentState', function($scope, $notification, $http, socket, currentState){

        $scope.oneAtATime = true;

        $scope.getBoards = function(){
            socket.emit('boards');
        };

        $scope.updateName = function(board, name){
            board.name = name;
            console.log("new name: " + name);
            socket.emit('setName', board);
            $notification.success('Name Updated With Success');
        };

        $scope.lightLed = function(board){
            board.led = 1 - board.led;
            var state = board.led === 1 ? "on" : "off";
            $http.get('http://' + board.ip + '/led?param=' + state);
            console.log('board: ' + board.ip + ' ledState: ' + state);
        };

        $scope.finishSetup = function(){
            currentState.finishSetup();
        };

        socket.on('boards', function(data){
            for(var i = 0; i < data.length; i++){
                data[i].led = 0;
            }
            $scope.boards = data;
            currentState.setBoards(data);
            $scope.$apply();
        });
    }])
    .controller('sessionsCtrl', ['$scope', 'socket', function($scope, socket){

        $scope.getSessions = function(){ socket.emit('sessionList') };

        $scope.loadSession = function(session){
            console.log("loading Session: " + session.id);
            socket.emit('getPrevSession', session.id);
        };

        $scope.config2 = {series:{pie: {show: true, radius: 1}}, legend: {show: true}};
        $scope.plotData = [];

        socket.on('sessionData', function(sessionData){
            $scope.sessionId = sessionData.session;
            var data = sessionData.data;
            $scope.plotData = [];
            $scope.pieData = [];
            for(var i = 0; i < data.length; i++){
                var sum = 0;
                var name = data[i].name == null || data[i].name === "" ? data[i].ip : data[i].name;
                console.log("name" + name+ " " + data[i].name);
                $scope.plotData[i] = [{"label":name, 'data':[], color: "#4572A7"}];
                for(var j = 0; j < data[i].speechData.length; j++){
                    $scope.plotData[i][0].data[j] = [j, data[i].speechData[j]];
                    sum += data[i].speechData[j];
                }
                $scope.pieData[i] = {'label':name, 'data':sum};
            }
        });

        socket.on('sessionList', function(sessions){
            $scope.sessions = sessions;
            $scope.$apply();
        })
    }])
    .controller('runCtrl', ['$scope', '$interval','socket', 'currentState', function($scope, $interval, socket, currentState){
        $scope.data1 = [{"label":'', 'data':[], color: "#4572A7"}];
        $scope.boards = currentState.getBoards();
        var lastData = function(){
            var dataContainer = {};
            var boards = currentState.getBoards();

            for(var i = 0; i < boards.length; i++){
                dataContainer[boards[i].ip] = {'prev': 0, 'data': [], 'name':boards[i].name};
            }
            return{
                'updateBoard': function(ip, pos){
                    dataContainer[ip].prev = pos;
                },
                'appendData': function(ip, toAppend){

                    for(var j = 0; j < toAppend.length; j++){
                        dataContainer[ip].data[dataContainer[ip].prev + j]
                            = [dataContainer[ip].prev + j, toAppend[j]];
                    }
                    dataState.updateBoard(ip, dataContainer[ip].data.length);
                },
                'getPos': function(){
                    var prevData = {};
                    for(var i = 0; i < boards.length; i++){
                        prevData[boards[i].ip] = dataContainer[boards[i].ip].prev;
                    }
                    return prevData;
                },
                'setPlot': function(ip){
                    $scope.data1[0].data = dataContainer[ip].data;
                    $scope.data1[0].label = dataContainer[ip].name;
                    console.log(dataContainer[ip].data);
                }
            }

        };
        console.log("session " + currentState.getSessionId());
        console.log("boards " + currentState.getBoards());
        var dataState = lastData();
        var getData = function() {
            socket.emit('getSession', {'id':currentState.getSessionId(), 'pos':dataState.getPos()});
        };
        $interval(getData, 2000);
        $scope.getRunningData = function(){ socket.emit('getSession', $scope.serverSession)};
        $scope.config1 = {
            yaxis: {ticks: [0, 1, 1.5]},
            series:{
                pie: {
                    show: true
                },
                lines:{
                    show: true
                }
            },
            legend: {
                show: true
            }};

        $scope.setData = function(ip){
            dataState.setPlot(ip);
        };

        socket.on('runningData', function(speechData){
            var data = speechData.data;
            for(var i = 0; i < data.length; i++){
                console.log('data ' + data[i].ip +" "+ data[i].speechData.length);
                dataState.appendData(data[i].ip, data[i].speechData);
            }
        });

    }])
    .controller('recordedCtrl', ['$scope', 'socket', function($scope, socket){
        $scope.getFiles = function(){
            socket.emit('getFiles');
        };
        socket.on('filesList', function(files){
            $scope.files = files;
            $scope.$apply();
        });

        $scope.loadFile = function(file){
            socket.emit('runFile', file);
        };
        $scope.config3 = {yaxis: {ticks: [0, 1, 1.5]}, series:{ pie: {show: true},lines:{show: true}}, legend: {show: true}};

        socket.on('fileResult', function(fileResult){
            console.log(fileResult);
            var boards = fileResult.boards;
            $scope.plotData = [];
            $scope.pieData = [];
            for(var i = 0; i < boards.length; i++){
                var sum = 0;
                var name = boards[i].name == null || boards[i].name === "" ? boards[i].ip : boards[i].name;
                $scope.plotData[i] = [{"label":name, 'data':[], color: "#4572A7"}];
                for(var j = 0; j < boards[i].data.length; j++){
                    $scope.plotData[i][0].data[j] = [j, boards[i].data[j]];
                    sum += boards[i].data[j];
                }
                $scope.pieData[i] = {'label':name, 'data':sum};
            }

            var generateGroup = function(group, time){
                var result = {'time':time};
                var name = "";
                for(var i = 0; i < group.length; i++){
                    if(i !== 0){
                        name += '|| ';
                    }
                    for(var j = 0; j < group[i].length; j++){
                        name += group[i][j];
                        name += " ";
                    }
                }
                result.name = name;
                return result;
            };

            var MagicPeriod = 0.057;

            var groupData = fileResult.hmmResult;
            $scope.groups = [];
            $scope.groups[0] = generateGroup(groupData[0],0);
            var currentGroup = $scope.groups[0].name;
            for(var i = 1; i < groupData.length; i++){
                var group = generateGroup(groupData[i], i*MagicPeriod);
                if(group.name !== currentGroup){
                    $scope.groups[$scope.groups.length] = group;
                    currentGroup = group.name;
                }
            }
            //console.log($scope.groups);
            $scope.$apply();
        })



    }]);