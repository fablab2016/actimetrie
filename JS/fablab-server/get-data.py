import threading
import requests
import json
import time
import sys
from pymongo import MongoClient

if len(sys.argv) != 2:
    print 'Expected 1 argument (session Id)'
    sys.exit()

sessionId = sys.argv[1]
boardsDict = {}
stop = False
client = MongoClient('localhost:27017')
db = client['actimetrie']

boards = [x['ip'] for x in db['boardcollection'].find({"session": sessionId})]
print sessionId
boardsChar = [str(unichr(x)) for x in range(ord('a'), ord('a') + len(boards))]
for i,ip in enumerate(boards):
    boardsDict[ip] = {'name': boardsChar[i], 'data':[]}

print boardsDict.keys()
print boardsDict.values()

def getData(ip, dataPos):
    try:
        r = requests.get("http://"+ip+"/data")
        jsonReq = json.loads(r.content)
        data = jsonReq["data"] #[1,0,1,1] if ip == '192.168.0.1' else [0,1,1,0]#
        print data
        for i,x in enumerate(data): 
            db['datacollection'].insert(
                {
                    "ip": ip,
                    "session": sessionId,
                    "position": dataPos,
                    "positionIndex": i,
                    "value": x 
                }
            )
            print x
    except ConnectionError:
        print ip + ' Not Responding'
    
def setupDataGet():
    for ip in boards:
        getData(ip)

threads = [0 for x in list(boards)]
preDataArray = {x:{} for x in list(boards)}

try:
    dataPos = 0
    while True:
        for ip in boards:
            threading.Thread(target = getData, args = [ip, dataPos]).start()
        dataPos += 1
        time.sleep(1.5)
except KeyboardInterrupt:
    pass
