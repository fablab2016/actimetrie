import threading
import requests
import json
import time
import sys
from pymongo import MongoClient

if(len(sys.argv) != 2)
    print 'Expected 1 argument (session Id)'
    sys.exit()

session = sys.argv[1]
groupConfig = "ab|cd" #example
boardsDict = {}
stop = False
client = MongoClient('localhost:27017')
db = client['actimetrie']

boardsChar = [str(unichr(x)) for x in range(ord('a'), ord('a') + len(boards))]
for i,ip in enumerate(boards):
    boardsDict[ip] = {'name': boardsChar[i], 'data':[]}

print boardsDict.keys()
print boardsDict.values()

def getData(ip, dataPos):
    print 'running: GET '+ "http://"+ip+"/data"
    try:
        r = requests.get("http://"+ip+"/data")
        jsonReq = json.loads(r.content)
        preDataArray[ip][str(dataPos)] = jsonReq["data"]
        print(jsonReq["data"])
    except ConnectionError:
        print ip + ' Not Responding'
    
def setupDataGet():
    for ip in boards:
        getData(ip)

threads = [0 for x in list(boards)]
preDataArray = {x:{} for x in list(boards)}

try:
    dataPos = 0
    while True:
        for ip in boards:
            threading.Thread(target = getData, args = [ip, dataPos]).start()
        dataPos += 1
        time.sleep(1)
except KeyboardInterrupt:
    pass

finalDataArray = {x:[] for x in list(boards)}

for ip in boards:
    dataDict = preDataArray[ip]
    dataIndex = 0  
    loop = True
    while str(dataIndex) in dataDict:
        finalDataArray[ip].extend(dataDict[str(dataIndex)]);
        dataIndex += 1


for x in finalDataArray.keys():
    print x
    print finalDataArray[x]

emissions = []
emissionIndex = 0;
done = False

while not done:
    emissionString = []
    done = True
    for ip in boards:
        if(emissionIndex < len(finalDataArray[ip])):
            done = False
            if(finalDataArray[ip][emissionIndex] == 1):
                emissionString.append(boardsDict[ip]['name'])
    stringResult = ""
    for name in sorted(list(emissionString)):
        stringResult += name
    emissions.append(stringResult)
    emissionIndex += 1

print emissions
print "done"
