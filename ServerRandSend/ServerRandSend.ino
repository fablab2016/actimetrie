#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>

const char* ssid = "mcb2";
const char* password = "abc123abc";

#define MAX_SPEAKING_VECTOR 100

int getDataCount = 0;
int mInd = 0;
int sum = 0;
int maxValue = 0;
int speaking[MAX_SPEAKING_VECTOR];
int speakingIndex = 0;

ESP8266WebServer server(80);

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void){
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/data", [](){
    char buffer[50 + MAX_SPEAKING_VECTOR*10];
    StaticJsonBuffer<50 + MAX_SPEAKING_VECTOR*10> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonArray& data = root.createNestedArray("data");
    for(int j = 0; j < speakingIndex; j++){
      data.add(speaking[j]);
    }
    speakingIndex = 0;
    root.printTo(buffer, root.measureLength() + 1);
    server.send(200, "application/json", buffer);    
  });

  server.on("/led", [](){
    String state = server.arg("param");
    if(state == "on") digitalWrite(LED_BUILTIN, LOW);
    else if(state == "off") digitalWrite(LED_BUILTIN, HIGH);
    server.send(200, "text/plain", "Led is now " + state);
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
  
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // print out the value you read:
  getDataCount++;
  sum = sum + sensorValue;
  if(sensorValue > maxValue) {
    maxValue = sensorValue;
  }
  if(getDataCount > 50) {
    if(speakingIndex < MAX_SPEAKING_VECTOR){      
        speaking[speakingIndex] = random(0,2);
      speakingIndex++;
    }
    getDataCount = 0;  
    maxValue = 0;
  }
  delay(1); 
}
