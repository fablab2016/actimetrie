import json

sessionData = 0
with open('JS/fablab-server/hmm-test') as json_data:
    sessionData = json.load(json_data)

final = []
for groups in sessionData['hmmResult']:
    gName = ""
    for group in groups:
        if gName != "":
            gName += "|"
        for member in group:
            gName += member
    final.append(gName)

i = 0.0
for data in final:
    print str(i) + " " + data
    i += 0.058
