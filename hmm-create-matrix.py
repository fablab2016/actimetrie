import numpy as np
from hmmlearn import hmm
import itertools

#TODO consult from DB
def getNbBoards():
    return 4

SELF_TRANSITION_PROB = 0.85
SIMULTANEOUS_TALK_COEF = 0.25
DEBUG = True

boards = [str(unichr(x)) for x in range(ord('a'), ord('a') + getNbBoards())]
states = {}
emissions = []
statesDict = []

#Join group names for states dict key
def getStateName(group):
    name = '';
    group.sort() # sort for std names over groups with same elements
    for i in range(0, len(group)):
        name += group[i]
        if i < len(group) - 1:
            name += '|'
    return name

#Verify possible repetitions
def validElements(element, group):
    for x in group:
        for elChar in list(element):
            for gChar in list(x):
                if elChar == gChar:
                    return False
    return True 

#Combines an element with all possible from other groups
def combine(groupUntilNow, maxSize, groups):
    curSize = sum(len(x) for x in groupUntilNow)
    for i in range(2, maxSize + 1): 
        if (curSize + i) > getNbBoards(): # cant find match if bigger than nb of boards, so just breaks
            break;
        if (curSize + i) != (getNbBoards() - 1):    # no elements alone in groups
            for x in groups[i]:
                if validElements(x, groupUntilNow): # validate for no repetition between groups
                    copy = list(groupUntilNow)
                    copy.append(x);
                    if (curSize + i) == getNbBoards():  # if exact match just add to final dict
                        states[getStateName(copy)] = copy
                    else:                               # else new recursion with new current group
                        combine(copy, maxSize - i, groups)

#Create group containing all elements
def addGroupWithAll(states):
    group = ''
    for x in boards:
        group += x
    states[group] = [group]

def simultaneousTalking(groups, emission):
    for group in groups:
        simSpeech = False
        for eChar in list(emission):
            if (eChar in group):
                if simSpeech:
                    return True
                else:
                    simSpeech = True

    return False
    
#Calculate not simultaneous talk probability based on SIMULTANEOUS_TALK_COEF
# each simultaneous prob is (coef)% of the not_simultaneous
def calculateNotSimProb(total, simultaneous):
    balancedAmounts = (total-simultaneous) + SIMULTANEOUS_TALK_COEF*simultaneous
    print balancedAmounts
    return 1/balancedAmounts

#Create the vector of emission probabilities for a given group
def getEmissionProb(groups):
    probArray = np.zeros(len(emissions));
    simTalkIndexes = []
    for i in range(0,len(emissions)):
        if(simultaneousTalking(groups, emissions[i])):
            simTalkIndexes.append(i) 

    notSimTalkProb = calculateNotSimProb(len(emissions), len(simTalkIndexes))
    if len(simTalkIndexes) != 0: 
        simTalkProb = (1.0 - notSimTalkProb*(len(emissions) - len(simTalkIndexes)))/(len(simTalkIndexes))
    
    probArray.fill(notSimTalkProb)
    for i in simTalkIndexes:
        probArray[i] = simTalkProb

    return probArray

def generateStateProb(nbBoards):
    groups = {}
    for i in range(2,nbBoards - 1):     #Generate combinations of elements to be used
        elts = [''.join(x) for x in itertools.combinations(boards,i)]
        groups[i] = elts        #Dict    key = Nb of elements used in combination; 
                                #        value = vector with all combinations 
    
    addGroupWithAll(states)
    for i in range(2, nbBoards - 1):
        for x in groups[i]:
            combine([x], nbBoards - i, groups)

    for i in range(1,nbBoards + 1):     #Generate combinations of possible emissions
        elts = [''.join(x) for x in itertools.combinations(boards,i)]
        emissions.extend(elts);  

    emissions.append('')

    arraySize = len(states.keys())
    #Create transition matrix
    #For now diagonal is constant and the rest just adjusts to it
    transitionProb = np.zeros((arraySize, arraySize), dtype=np.float)
    otherTransitions = (1 - SELF_TRANSITION_PROB)/(arraySize-1) #TODO change prob dynamically
    transitionProb.fill(otherTransitions)
    np.fill_diagonal(transitionProb, SELF_TRANSITION_PROB)

    #Create emission probability matrix
    emissionProb = np.zeros((arraySize, len(emissions)), dtype=np.float)
    i = 0
    for groups in states.values():
        emissionRow = getEmissionProb(groups)
        emissionProb[i] = emissionRow
        i += 1

    for i in range(0, len(states.keys())):
        statesDict.append(states.keys()[i])
        
    if(DEBUG):
        print "Transition Matrix"    
        print transitionProb

        print "Emission Matrix"
        print emissionProb

        print "Values, Keys and Emissions"
        print (sorted(states.values()))
        print (sorted(states.keys()))
        print (emissions)
    return [transitionProb, emissionProb]

    

emissionTest = 
#['abcd','a','ac', 'abcd','bc', 'ab', 'cd', 'a','a','a', 'ac', 'abcd', 'abc', 'ac','ac','ac','ac', 'ad', 'ad', 'abd','c', 'd', 'ad','ad','b', 'b','b','b','bc', 'bc','bc','abcd','bc','bc', 'ab', 'ab','ab','ab','ab','ab', 'cd', 'bd','ac' ,'ac','ac','ac','ad', 'ac','ac', 'bd','bd','bd','bd','bd','bd']

nbBoards = getNbBoards()
initProb = np.zeros((nbBoards))
initProb.fill(1.0/nbBoards)

if(nbBoards < 3):
    print('You need a min of 4 Boards to form more than 1 group')

result = generateStateProb(nbBoards)

convertedObs = []
for i in range(0, len(emissionTest)):
    convertedObs.append([emissions.index(emissionTest[i])])
print 'INPUT - Observations'
print emissionTest
print convertedObs

model = hmm.MultinomialHMM(n_components=len(states.keys()))
model.startprob_ = initProb
model.transmat_ = result[0]
model.emissionprob_ = result[1]
prediction = model.predict(convertedObs)

convertedStates = []
for i in range(0, len(prediction)):
    convertedStates.append(statesDict[prediction[i]])
print 'OUTPUT - States'
print prediction
print convertedStates




   
